import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from 'src/app/auth-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private authService: AuthServiceService) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(){
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required])
    })
  }
  registerProcess(){
    if(this.formGroup.valid){
      const formData = new FormData();
      formData.append('name', this.formGroup.get('name').value);
      formData.append('username', this.formGroup.get('username').value);
      formData.append('password', this.formGroup.get('password').value);

      this.authService.register(formData).subscribe(result => {
        console.log(result);
      })
    }
  }
}
